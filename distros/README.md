# distro buildscripts
you should be able to build a "latest" distro image with suitable linux-wiiu customisation by
running the -bootstrap scripts in each folder.

the main requirement is that your runner should be able to be true root (need to make /dev nodes)
and you should be able to chroot into a `ppc` system (either run the script on ppc hardware, or use
`qemu-user-binfmt` to enable emulation).

on debian, just install `qemu-user-binfmt` (and `deboostrap`, `parted` for debian builds), reboot,
and away you go. I do my builds on Fedora so I know it's possible to do an emulation setup there too.

`apk` is needed for Adélie builds. you can use `adelie-bootstrap-tarball.sh` if that isn't possible
for you.

usually you have to delete the distro/rootfs folder between runs. keep `cache` if it exists since
this will save on download time between runs
