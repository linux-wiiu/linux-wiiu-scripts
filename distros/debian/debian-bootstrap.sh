#!/bin/bash
set -euo pipefail

{

DEBIAN=http://deb.debian.org/debian-ports/

EXTRAPKG=debian-ports-archive-keyring,dbus,dbus-user-session,sudo,wget,fbset,parted,fdisk,network-manager,bluetooth,console-setup,systemd
EXCLDPKG=powerpc-utils,powerpc-ibm-utils

echo "==> Installing..."

mkdir -p rootfs cache
curl https://www.ports.debian.org/archive_`date +%Y`.key | gpg --no-default-keyring --keyring=./debian.gpg --import -
debootstrap --arch=powerpc --keyring=./debian.gpg --cache-dir=$(pwd)/cache \
 --include=$EXTRAPKG --exclude=$EXCLDPKG --extra-suites=unreleased \
 unstable ./rootfs $DEBIAN

#let's go!
echo "==> Setting up distro..."

echo "  > Adding motd..."
cat ../motd >> ./rootfs/etc/motd

echo "  > Adding template fstab..."
cp ../fstab ./rootfs/etc/fstab

echo "  > Boosting font size..."
sed -i "s/FONTFACE=\"Fixed\"/FONTFACE=\"TerminusBold\"/g" ./rootfs/etc/default/console-setup
sed -i "s/FONTSIZE=\"8x16\"/FONTSIZE=\"14x28\"/g" ./rootfs/etc/default/console-setup
# wild how this isn't the default. love it, debian
sed -i "s/CHARMAP=\"ISO-8859-15\"/CHARMAP=\"UTF-8\"/g" ./rootfs/etc/default/console-setup

echo "  > Adding rootfs_enlarge..."
mkdir -p ./rootfs/usr/local/share/linux-wiiu
cp ../rootfs_enlarge.sh ./rootfs/usr/local/share/linux-wiiu/rootfs_enlarge.sh
cp ./fix-repo.sh ./rootfs/usr/local/share/linux-wiiu/fix-repo.sh
chmod +x ./rootfs/usr/local/share/linux-wiiu/rootfs_enlarge.sh
chmod +x ./rootfs/usr/local/share/linux-wiiu/fix-repo.sh

echo "  > Changing hostname..."
echo "wiiu" > ./rootfs/etc/hostname
sed -i "s/\tlocalhost/\tlocalhost `cat ./rootfs/etc/hostname`/g" ./rootfs/etc/hosts

echo "  > Entering system..."
cat << ch_EOF | chroot ./rootfs/
source /etc/profile

useradd wiiu -s /bin/bash -m
cat << EOF | chpasswd
root:wiiu
wiiu:wiiu
EOF
chage -d 0 root
chage -d 0 wiiu

apt-get clean

ch_EOF

echo "  > Install done!"
echo "==> Packaging tarball..."

TARBALLNAME=debian-wiiu-`date -I`

cd ./rootfs/
tar --preserve-permissions --acls --xattrs --sparse -cJf ../$TARBALLNAME.tar.xz .
cd ../

echo "==> Done! $TARBALLNAME.tar.xz"

../make_image.sh ./rootfs/ ./$TARBALLNAME

} | tee debian.log
