#!/usr/bin/env bash
set -euo pipefail

ROOTFS=$1
KTYPE=$2

if [ "$KTYPE" = "smp" ]; then
    KERNEL=https://gitlab.com/linux-wiiu/linux-wiiu/-/jobs/9370045021/artifacts/download
    KVER=6.6.80-wiiusmp-g22d704b3cd3e
else
    KERNEL=https://gitlab.com/linux-wiiu/linux-wiiu/-/jobs/9369912512/artifacts/download
    KVER=6.6.80-wiiu-g135fdcd76b5c
fi

echo "  > Downloading kernel $KVER..."
curl -L -o cache/kernel.zip "$KERNEL"
mkdir -p cache/kernel
unzip -o cache/kernel.zip -d cache/kernel

echo "  > Installing..."
mkdir -p "$ROOTFS/boot"
echo "$KVER" > "$ROOTFS/boot/vmlinux.kver"
cp cache/kernel/vmlinux.strip.gz "$ROOTFS/boot/vmlinux"
cp cache/kernel/wiiu.dtb "$ROOTFS/boot/wiiu.dtb"
tar xpf cache/kernel/modules.tar.gz -C "$ROOTFS"

OSNAME=$( source "$ROOTFS/etc/os-release"; echo "$PRETTY_NAME"; )

echo "  > Writing Petitboot entry for $OSNAME..."
cat > "$ROOTFS/boot/petitboot.conf" << EOF
name   $OSNAME
image  /boot/vmlinux
dtb    /boot/wiiu.dtb
args   root=/dev/sda1
EOF