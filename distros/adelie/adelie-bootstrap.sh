#!/bin/bash
set -euo pipefail

{

ARCH=espresso
KEYS=(
	"https://packages.atwilcox.tech/adelie-tier3/keys/espresso@packages.atwilcox.tech.pub"
	"https://packages.atwilcox.tech/adelie-tier3/keys/ppc64@packages.foxkit.us.pub"
)
REPOS=(
	"https://packages.atwilcox.tech/adelie-tier3/current/system"
	"https://packages.atwilcox.tech/adelie-tier3/current/user"
)
# from https://git.adelielinux.org/adelie/image/-/blob/current/configs/base/base.installfile?ref_type=heads
PKGS=(
	adelie-base-posix dash-binsh ssmtp s6-linux-init s6-linux-init-early-getty openrc eudev sysklogd
	parted cryptsetup diskdev_cmds dosfstools e2fsprogs jfsutils lvm2 mdadm xfsprogs-base
	dracut no-boot
	ca-certificates curl dhcpcd iproute2 iputils mtr net-tools netifrc netifrc-doc s6-dns traceroute
	iw wireless-tools wpa_supplicant
	hdparm pciutils pcmciautils smartmontools usbutils
	bzip2 chrony gnupg kbd-keymaps less links lzop nano openssh tmux
	docs

	# extras
	bluez networkmanager
	libusbmuxd libimobiledevice
)

if ! command -v apk 2>&1 >/dev/null
then
	>&2 echo "apk does not seem to be installed! Please run this script on an Adélie Linux system."
	exit 1
fi

echo "==> Initialising target..."
mkdir -p rootfs/etc/apk/keys
mkdir -p cache
pushd rootfs > /dev/null
target_apk() {
	apk --arch $ARCH --root . --cache-dir ../cache "$@"
}

# needed for some package install scripts
mkdir -p dev
mount --rbind /dev dev/
mount --make-rslave dev/
{ # /dev is mounted
trap ">&2 echo RUN '[sudo] umount -l ./rootfs/dev/' NOW" ERR SIGINT SIGTERM SIGQUIT

curl --output-dir etc/apk/keys --remote-name-all "${KEYS[@]}"
target_apk --initdb add
printf "%s\n" "${REPOS[@]}" > etc/apk/repositories

echo "==> Installing system..."
target_apk update
target_apk add "${PKGS[@]}"

umount -l dev/
trap - ERR SIGINT SIGTERM SIGQUIT
}
popd > /dev/null

echo "==> Installing kernel..."
../install_kernel.sh ./rootfs smp
KVER=$(cat rootfs/boot/vmlinux.kver)

#let's go!
echo "==> Setting up distro..."

echo "  > Adding template fstab..."
cp ../fstab ./rootfs/etc/fstab

echo "  > Adding rootfs_enlarge..."
mkdir -p ./rootfs/usr/local/share/linux-wiiu
cp ../rootfs_enlarge.sh ./rootfs/usr/local/share/linux-wiiu/rootfs_enlarge.sh
chmod +x ./rootfs/usr/local/share/linux-wiiu/rootfs_enlarge.sh

echo "  > Changing hostname..."
echo "wiiu" > ./rootfs/etc/hostname
sed -i "s/\tlocalhost/\tlocalhost $(cat ./rootfs/etc/hostname)/g" ./rootfs/etc/hosts

echo "  > Changing root shell..."
sed -i "s#:/root:/bin/sh#:/root:/bin/zsh#g" ./rootfs/etc/passwd

echo "  > Entering system..."
cat << ch_EOF | chroot ./rootfs/ /bin/bash -i
source /etc/profile

useradd wiiu -s /bin/zsh -m
cat << EOF | chpasswd
root:wiiu
wiiu:wiiu
EOF
chage -d 0 root
chage -d 0 wiiu

rc-update add sysklogd default
rc-update add udev boot
rc-update add udev-trigger boot
rc-update add elogind default
rc-update add wpa_supplicant default
rc-update add NetworkManager default

dracut --kver $KVER
echo "initrd /boot/initramfs-$KVER.img" >> /boot/petitboot.conf

ch_EOF

echo "  > Install done!"
echo "==> Packaging tarball..."

TARBALLNAME=adelie-wiiu-$(date -I)

cd ./rootfs/
tar --preserve-permissions --acls --xattrs -cJf "../$TARBALLNAME.tar.xz" .
cd ../

echo "==> Done! $TARBALLNAME.tar.xz"

../make_image.sh ./rootfs/ "./$TARBALLNAME" || echo "make_image failed, skipping. To try again, run [sudo] ../make_image.sh ./rootfs/ ./$TARBALLNAME"

} | tee adelie.log
