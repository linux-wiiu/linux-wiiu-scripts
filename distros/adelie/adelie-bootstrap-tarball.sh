#!/bin/bash
set -euo pipefail

{

echo "==> Downloading tarball..."

ADELIE=https://distfiles.adelielinux.org/adelie/current/iso
SUMS=SHA512SUMS

mkdir -p cache
pushd cache > /dev/null
curl -o $SUMS $ADELIE/$SUMS

regex='adelie-rootfs-full-ppc-\S*.txz'
sum=$(grep $regex $SUMS)
file=$(grep -o $regex $SUMS)

if echo $sum | sha512sum --check --status
then
	echo "  > Cache OK!"
else
	echo "  > Downloading $file..."
	rm -f $file
	curl -o $file $ADELIE/$file
	echo $sum | sha512sum --check
fi
popd > /dev/null

echo "==> Installing..."

mkdir -p rootfs
tar -xP --acls --xattrs --same-owner --same-permissions --numeric-owner --sparse -C rootfs -f cache/$file

#let's go!
echo "==> Setting up distro..."

echo "  > Adding template fstab..."
cp ../fstab ./rootfs/etc/fstab

echo "  > Adding rootfs_enlarge..."
mkdir -p ./rootfs/usr/local/share/linux-wiiu
cp ../rootfs_enlarge.sh ./rootfs/usr/local/share/linux-wiiu/rootfs_enlarge.sh
chmod +x ./rootfs/usr/local/share/linux-wiiu/rootfs_enlarge.sh

echo "  > Changing hostname..."
echo "wiiu" > ./rootfs/etc/hostname
sed -i "s/\tlocalhost/\tlocalhost `cat ./rootfs/etc/hostname`/g" ./rootfs/etc/hosts

echo "  > Entering system..."
cat << ch_EOF | chroot ./rootfs/
source /etc/profile

useradd wiiu -s /bin/bash -m
cat << EOF | chpasswd
root:wiiu
wiiu:wiiu
EOF
chage -d 0 root
chage -d 0 wiiu

ch_EOF

echo "  > Install done!"
echo "==> Packaging tarball..."

TARBALLNAME=adelie-wiiu-`date -I`

cd ./rootfs/
tar --preserve-permissions --acls --xattrs --sparse -cJf ../$TARBALLNAME.tar.xz .
cd ../

echo "==> Done! $TARBALLNAME.tar.xz"

../make_image.sh ./rootfs/ ./$TARBALLNAME

} | tee adelie.log
