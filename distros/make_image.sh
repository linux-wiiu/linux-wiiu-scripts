#!/usr/bin/env bash
set -euo pipefail

# image creation code is modified from https://github.com/theofficialgman/l4t-image-buildscripts/blob/master/scripts/create_image.sh
# then by Techflash for Wii Linux (thanks!!)
# then ashquarky for linux-wiiu

ROOTFS=$1
OUTPUT_PART=$2.part.img
OUTPUT_DISK=$2.disk.img

echo "==> Creating filesystem..."

bytes="$(du -sb $ROOTFS | awk '{ print $1 }')"
# buffer space of 128MB
partmibs=$((bytes / (1024*1024) + 128))
echo "  > Creating $((partmibs))MiB disk image..."
dd if=/dev/zero of=$OUTPUT_PART bs=1MiB count=$((partmibs))

mkfs.ext4 -O ^metadata_csum,^64bit $OUTPUT_PART
e2label $OUTPUT_PART linux-wiiu

uuid=$(blkid -s UUID -o value $OUTPUT_PART)

mkdir -p mnt/
mount $OUTPUT_PART mnt/

echo "  > Copying files..."
cp -Pra $ROOTFS/* mnt/ || {
	echo "Failed to copy files - out of disk space?"
	umount mnt/
	exit 1
}

# cheeky hack
echo "  > Fixing up fstab..."
sed -i "s#/dev/sda1#UUID=$uuid#g" mnt/etc/fstab
sed -i "s#/dev/sda1#UUID=$uuid#g" mnt/boot/petitboot.conf || true

sync
sleep 1

umount mnt/
rmdir mnt/

echo "  > Tidying up image..."
zerofree $OUTPUT_PART

echo "==> Done! $OUTPUT_PART"

echo "==> Creating disk image..."
# and another 8MB for the partition table stuffs
dd if=/dev/zero of=$OUTPUT_DISK bs=1MiB count=$((partmibs + 8))
parted $OUTPUT_DISK mklabel msdos
parted $OUTPUT_DISK mkpart primary ext4 1MiB $((partmibs + 1))MiB

echo "  > Copying partition..."
dd if=$OUTPUT_PART of=$OUTPUT_DISK bs=1MiB seek=1
sync

parted $OUTPUT_DISK print
echo "==> Done! $OUTPUT_DISK"
